# Boilerplate of Dreamz #

Has all of the things, well mostly just these things:

- webpack v3
- webpack dev middleware
- hot module reloading (including React)
- Babel
- React
- Redux
- React Router v4
- Eslint
- CSS Modules
- Sass

## Installing it
`npm install`

## Running it
`npm run watch`
For all your development needs, runs webpack-dev-middleware via node.js

## Build it
`npm run build`
For all your production needs, creates static assets that are crunched down super small

## Releasing to a server
`npm run build && npm start`
