import { combineReducers } from 'redux';

import geolocation from './geolocation';

const reducers = combineReducers({
  geolocation
});

export default reducers;
