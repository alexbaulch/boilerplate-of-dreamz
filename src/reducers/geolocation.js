const initialState = {
  error: null,
  loading: false,
  location: {
    coords: {
      latitude: 51.5074,
      longitude: 0.1278
    }
  }
};


const applePay = (state = initialState, action) => {
  switch (action.type) {
    case 'REQUEST_GEOLOCATION':
      return Object.assign({}, state, { loading: true });
    case 'REQUEST_GEOLOCATION_SUCCESS':
      return Object.assign({}, state, { loading: false, location: action.location });
    case 'REQUEST_GEOLOCATION_FAILURE':
      return Object.assign({}, state, { error: action.error, loading: false, location: null });
    default:
      return state;
  }
};

export default applePay;
