export function requestGeolocation() {
  return {
    type: 'REQUEST_GEOLOCATION'
  };
}

export function requestGeolocationSuccess(location) {
  return {
    type: 'REQUEST_GEOLOCATION_SUCCESS',
    location
  };
}

export function requestGeolocationFailure(error) {
  return {
    type: 'REQUEST_GEOLOCATION_FAILURE',
    error
  };
}

export function getGeolocation(options) {
  return (dispatch) => {
    dispatch(requestGeolocation());
    return new Promise((resolve, reject) => {
      navigator.geolocation.getCurrentPosition(resolve, reject, options);
    });
  };
}
