import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import PropTypes from 'prop-types';

import Home from '../Home/Container';
import ErrorPage from '../ErrorPage/';

class App extends Component {
  static propTypes = {
    getGeolocation: PropTypes.func.isRequired,
    requestGeolocationFailure: PropTypes.func.isRequired,
    requestGeolocationSuccess: PropTypes.func.isRequired
  }

  constructor(props) {
    super(props);

    props.getGeolocation()
      .then((location) => {
        props.requestGeolocationSuccess(location);
      })
      .catch((error) => {
        props.requestGeolocationFailure(error);
      });
  }

  render() {
    return (
      <div className="app">
        <Router>
          <Switch>
            <Route component={Home} exact path="/" />
            <Route component={ErrorPage} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
