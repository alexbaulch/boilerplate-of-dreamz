import { connect } from 'react-redux';
import { withRouter } from 'react-router';

import { getGeolocation, requestGeolocationFailure, requestGeolocationSuccess } from '../../actions/geolocation';

import App from './index';

const mapDispatchToProps = dispatch => ({
  getGeolocation: () => dispatch(getGeolocation()),
  requestGeolocationFailure: error => dispatch(requestGeolocationFailure(error)),
  requestGeolocationSuccess: location => dispatch(requestGeolocationSuccess(location))
});

export default withRouter(connect(null, mapDispatchToProps)(App));
