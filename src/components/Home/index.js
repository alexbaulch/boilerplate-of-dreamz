import React, { Component } from 'react';
import PropTypes from 'prop-types';

import styles from './home.scss';

class Home extends Component {
  static propTypes = {
    lat: PropTypes.number.isRequired,
    lng: PropTypes.number.isRequired
  }

  render() {
    const { lat, lng } = this.props;

    return (
      <div className={styles.root}>
        <p>Home page</p>
        <p>Your location is... latitude: {lat}, longitude: {lng}</p>
      </div>
    );
  }
}

export default Home;
