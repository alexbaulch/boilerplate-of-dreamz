import { connect } from 'react-redux';

import Home from './index';

const mapStateToProps = state => ({
  lat: state.geolocation.location.coords.latitude,
  lng: state.geolocation.location.coords.longitude
});

export default connect(mapStateToProps)(Home);
