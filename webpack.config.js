const path = require('path');
const webpack = require('webpack');
const merge = require('webpack-merge');

// webpack plugins
const CleanWebpackPlugin = require('clean-webpack-plugin');

const parts = require('./webpack.parts');

const IS_PRODUCTION = process.env.NODE_ENV === 'production';
const PATHS = {
  app: path.join(__dirname, 'src', 'index.js'),
  build: path.join(__dirname, 'public', 'build')
};

const commonConfig = merge([
  {
    output: {
      path: PATHS.build,
      publicPath: '/public/build/',
      filename: '[name].js'
    },
    stats: {
      hash: false,
      version: false,
      children: false,
    },
    resolve: {
      extensions: ['.svg', '.scss', '.js'],
      modules: ['node_modules', 'client', 'public'],
    }
  },
  parts.defineGlobals({
    'process.env.NODE_ENV': process.env.NODE_ENV || 'development'
  }),
  parts.lintJavaScript({
    exclude: [/node_modules/]
  }),
  parts.loadJavaScript({
    exclude: [/node_modules/, /vendor/],
    isDev: !IS_PRODUCTION
  }),
  parts.loadSvg({ exclude: /node_modules/ })
]);

const devConfig = merge([
  {
    entry: {
      app: [
        'babel-polyfill',
        'whatwg-fetch',
        'react-hot-loader/patch',
        'webpack-hot-middleware/client?reload=true',
        PATHS.app
      ]
    }
  },
  parts.devTool('eval-source-map'),
  parts.devServer(),
  parts.loadStyles()
]);

const prodConfig = merge([
  {
    entry: {
      app: [
        'babel-polyfill',
        'whatwg-fetch',
        PATHS.app
      ]
    },
    output: {
      filename: '[name].js',
    },
    plugins: [
      new CleanWebpackPlugin([PATHS.build], {
        verbose: false
      }),
      new webpack.optimize.UglifyJsPlugin({
        sourceMap: true,
        compress: {
          warnings: false
        },
        output: {
          comments: false
        }
      })
    ]
  },
  parts.devTool('source-map'),
  parts.extractStyles()
]);

module.exports = merge(commonConfig, IS_PRODUCTION ? prodConfig : devConfig)
