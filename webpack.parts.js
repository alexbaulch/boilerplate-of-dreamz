const webpack = require('webpack');

const ExtractTextPlugin = require('extract-text-webpack-plugin');

exports.defineGlobals = (items) => {
  const globalConsts = Object.keys(items).reduce((obj, key) => {
    obj[key] = JSON.stringify(items[key]);
    return obj;
  }, {});

  return {
    plugins: [
      new webpack.DefinePlugin(globalConsts)
    ]
  }
};

exports.devServer = () => ({
  devServer: {
    publicPath: '/public/build/',
    historyApiFallback: true,
    hot: true,
    stats: 'errors-only',
    overlay: {
      errors: true,
      warnings: true
    }
  },
  plugins: [
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NamedModulesPlugin(),
  ]
});

exports.devTool = (devtool) => ({
  devtool,
});

exports.extractStyles = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: [
            {
              loader: 'css-loader',
              options: {
                modules: true,
                importLoaders: 2,
                localIdentName: '[name]__[local]___[hash:base64:5]'
              }
            },
            'sass-loader',
            'postcss-loader'
          ]
        })
      }
    ]
  },
  plugins: [
    new ExtractTextPlugin({
      filename: '[name].css',
    })
  ]
})

exports.lintJavaScript = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        include,
        exclude,
        enforce: 'pre',
        use: 'eslint-loader',
      }
    ]
  }
});

exports.loadJavaScript = ({ isDev, include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.js$/,
        include,
        exclude,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: isDev,
          }
        }
      }
    ]
  }
});

exports.loadStyles = ({ include, exclude } = {}) => ({
  module: {
    rules: [
      {
        test: /\.scss$/,
        use: [
          'style-loader',
          {
            loader: 'css-loader',
            options: {
              modules: true,
              importLoaders: 2,
              localIdentName: '[name]__[local]___[hash:base64:5]'
            }
          },
          'sass-loader',
          'postcss-loader'
        ]
      }
    ]
  }
});

exports.loadSvg = ({ include, exclude }) => ({
  module: {
    rules: [
      {
        test: /\.svg$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          {
            loader: 'react-svg-loader',
            query: {
              jsx: true
            }
          }
        ]
      }
    ]
  }
});
