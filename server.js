// require node modules
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const chalk = require('chalk');
const path = require('path');
const fs = require('fs');
const app = express();
const PORT = process.env.PORT || 3000;
const server = require('http').Server(app);
require('dotenv').config({path: path.resolve(__dirname, './.env')});

// require local modules
const apiRoutes = require('./api/v1');

// set app locals
app.locals.isProduction = process.env.NODE_ENV === 'production';

// Webpack dev server
if (!app.locals.isProduction) {
  const webpack = require('webpack');
  const webpackConfig = require('./webpack.config');
  const compiler = webpack(webpackConfig);

  app.use(require('webpack-dev-middleware')(compiler, webpackConfig.devServer));
  app.use(require('webpack-hot-middleware')(compiler, { reload: true }));
}

// Start the server
server.listen(PORT, () => console.log(`ScanPayGo is listening on port ${PORT}`));

// Log request
app.use((req, res, next) => {
  console.log(`${chalk.magenta.bold(req.method)}${(req.method.length < 5 ? ' ' : '')}`, chalk.bold(req.originalUrl));
  next();
});

// allow Content-Type to be specified
app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Headers', 'Content-Type, X-Requested-With');
  next();
});

// handle json data, urlencoded data and cookies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

// API
app.use('/api/v1', apiRoutes);

// Serve static assets
app.use('/public', express.static('public'));

// Return the HTML file for all requests
app.use((req, res) => {
  res.sendFile(path.join(__dirname + '/index.html'));
});
