var express = require('express');
var router = express.Router();

const products = require('./products');

router.use('/products', products);

module.exports = router;
